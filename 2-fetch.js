const usersAPI = fetch("https://jsonplaceholder.typicode.com/users")
const todosAPI = fetch("https://jsonplaceholder.typicode.com/todos")


// 1. Fetch all the users
fetchUsers = () =>{

    usersAPI.then(response => {
        if (response.ok) {
            return (response.json())
        }
        else {
            console.log("Unsuccessful");
        }
    }).then(response => {
        console.log(response);
    }).catch(err => {
        console.log("ERROR", err);
    });
}

fetchUsers();


// 2. Fetch all the todos
fetchTodos =  () => {
    todosAPI.then(response => {
        if (response.ok) {
            return (response.json())
        }
        else {
            console.log("Unsuccessful");
        }
    }).then(response => {
        console.log(response);
    }).catch(err => {
        console.log("ERROR", err);
    });
}


fetchTodos();













